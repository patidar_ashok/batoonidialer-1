package com.batooni.permission;

import com.android.contacts.common.util.PermissionsUtil;

import java.util.ArrayList;

public class PermissionItem {


    public String permissionName;
    public ArrayList<String> permissions;

    public ArrayList<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<String> permissions) {
        this.permissions = permissions;
    }

    public PermissionItem(String permission, ArrayList<String> permissions) {
        this.permissionName = permission;
        this.permissions = permissions;
    }



    public String getTitle() {
        String title ="default";
        if(permissionName == PermissionsUtil.DEFAULT_DIALER)
            return "Permissions";
        else if(permissionName == PermissionsUtil.REBOOT)
            return "Start after reboot";
        else if(permissionName == PermissionsUtil.WRITE_SETTINGS)
            return "Write Setting";
        else if(permissionName == PermissionsUtil.NOTIFICATION_ACCESS)
            return "Access to notifications";
        else
            return title;
    }



    public String getBrief() {
        String brief ="default";
        if(permissionName == PermissionsUtil.DEFAULT_DIALER)
            return "So you can make calls and access your contacts through batooni";
        else if(permissionName == PermissionsUtil.REBOOT)
            return "Allow Batooni to work after reboot";
        else if(permissionName == PermissionsUtil.WRITE_SETTINGS)
            return "Allow Write Setting";
        else if(permissionName == PermissionsUtil.NOTIFICATION_ACCESS)
            return "To see your incoming messages and calls together";


        return brief;
    }

    public int getRequestCode(){
        int requestCode = 0;
        if(permissionName == PermissionsUtil.DEFAULT_DIALER)
            return 101;
        else if(permissionName == PermissionsUtil.REBOOT)
            return 201;
        else if(permissionName == PermissionsUtil.WRITE_SETTINGS)
            return 301;

        return requestCode;
    }


}
