package com.batooni.permission;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.batooni.dialer.R;
import android.widget.CheckBox;

public class PermissionListAdapter extends BaseAdapter {

    private ArrayList<PermissionItem> listData;

    private LayoutInflater layoutInflater;

    private Context mContext;

    public PermissionListAdapter(Context context, ArrayList<PermissionItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.permission, null);
            holder = new ViewHolder();
            holder.headlineView = (TextView) convertView.findViewById(R.id.permission_name);
            holder.briefText = (TextView) convertView.findViewById(R.id.permission_brief);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.permission_checkout);
            holder.listdivider = (ImageView)convertView.findViewById(R.id.listdivider);
            holder.checkBox.setOnClickListener(new CheckBox.OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder.checkBox.setChecked(false);
                    ((PermissionActivity)mContext).onCheckBoxItemClick(holder.checkBox,position);
                }

            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PermissionItem permissionItem = (PermissionItem) listData.get(position);
        holder.headlineView.setText(permissionItem.getTitle());
        holder.briefText.setText(permissionItem.getBrief());
        holder.listdivider.setVisibility(View.VISIBLE);

        if(position == (listData.size()-1))
            holder.listdivider.setVisibility(View.GONE);



        return convertView;
    }

    static class ViewHolder {
        TextView headlineView;
        TextView briefText;
        CheckBox checkBox;
        ImageView listdivider;
    }
}
