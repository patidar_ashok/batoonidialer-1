package com.batooni.permission;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.telecom.TelecomManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.android.contacts.common.util.PermissionsUtil;
import com.batooni.dialer.DialerApplication;
import com.batooni.dialer.DialtactsActivity;
import com.batooni.dialer.ForceRequestPermissionsActivity;
import com.batooni.dialer.Log;
import com.batooni.dialer.TransactionSafeActivity;
import com.batooni.dialer.widget.SmoothCheckBox;
import android.net.Uri;

import com.batooni.dialer.R;

import java.util.ArrayList;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.provider.Settings;

import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;

public class PermissionActivity extends TransactionSafeActivity {

    private static final int PERMISSIONS_REQUEST_ALL_PERMISSIONS = 5;
    private static final int PERMISSION_REQUEST_DIALER_REPLACEMENT = 10;
    private static final int PERMISSION_REQUEST_CONTACTS = 11;
    private static final int PERMISSION_REQUEST_AUTO_START =12;
    private static final int PERMISSIONS_REQUEST_WRITE_SETTINGS =13;
    private static final int PERMISSIONS_REQUEST_NOTIFICATION_ACCESS =14;

    public static ArrayList<String>  unsatisfiedCallPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiedrebootPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiedsettingPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiednotificationPermissions = new ArrayList<>();

    @BindView(R.id.permissionlist)
    ListView permissionListView;

    CheckBox checkbox;
    PermissionItem permissionItem;

    PermissionListAdapter listAdapter;


    Button hopeinButton;


    ArrayList<PermissionItem> permissionItemList = new ArrayList<>();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission_activity);
        ButterKnife.bind(this);


        hopeinButton = findViewById(R.id.hopin);
        hopeinButton.setEnabled(false);

        hopeinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PermissionActivity.this, DialtactsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        if(!DialerApplication.unsatisfiedrebootPermissions.isEmpty()){
            permissionItemList.add(new PermissionItem(PermissionsUtil.REBOOT,DialerApplication.unsatisfiedrebootPermissions));
        }

        if(!DialerApplication.unsatisfiedCallPermissions.isEmpty()){
            permissionItemList.add(new PermissionItem(PermissionsUtil.DEFAULT_DIALER,DialerApplication.unsatisfiedCallPermissions));
        }

        /*if(!com.batooni.dialer.DialerApplication.unsatisfiedsettingPermissions.isEmpty() && !Settings.System.canWrite(this)){
            permissionItemList.add(new PermissionItem(com.android.contacts.common.util.PermissionsUtil.WRITE_SETTINGS,DialerApplication.unsatisfiedsettingPermissions));
        }

        if(!com.batooni.dialer.DialerApplication.unsatisfiednotificationPermissions.isEmpty() && !isNLServiceRunning()){
            permissionItemList.add(new PermissionItem(PermissionsUtil.NOTIFICATION_ACCESS,DialerApplication.unsatisfiednotificationPermissions));
        }*/

        if(permissionItemList.isEmpty()){
            Intent i = new Intent(this, DialtactsActivity.class);
            startActivity(i);
            finish();
        }

        listAdapter = new PermissionListAdapter(this,permissionItemList);
        permissionListView.setAdapter(listAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void onCheckBoxItemClick(CheckBox checkbox, int position) {

        this.checkbox = checkbox;
        permissionItem = permissionItemList.get(position);

        if(permissionItem.permissionName == PermissionsUtil.DEFAULT_DIALER){

                offerReplacingDefaultDialer(permissionItem.getRequestCode());
        }

        if (permissionItem.permissionName == PermissionsUtil.REBOOT){
            requestPermissions(GetStringArray(permissionItem.getPermissions()), PERMISSION_REQUEST_AUTO_START);
        }

        if (permissionItem.permissionName == PermissionsUtil.WRITE_SETTINGS){
            offerWriteingSettingPermission(PERMISSIONS_REQUEST_WRITE_SETTINGS);
        }

        if (permissionItem.permissionName == PermissionsUtil.NOTIFICATION_ACCESS){
            //offerNotificationAccessPermission(PERMISSIONS_REQUEST_NOTIFICATION_ACCESS);
            requestPermissions(GetStringArray(permissionItem.getPermissions()),PERMISSIONS_REQUEST_NOTIFICATION_ACCESS);
        }
    }


    private void offerReplacingDefaultDialer(int requestCode) {
        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

        if (!getPackageName().equals(telecomManager.getDefaultDialerPackage())) {
            Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER)
                    .putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(intent,PERMISSION_REQUEST_DIALER_REPLACEMENT);
        }else{

            offerContactReadAndWritePermission();

        }
    }

    private void offerContactReadAndWritePermission() {
        requestPermissions(GetStringArray(permissionItem.getPermissions()),PERMISSION_REQUEST_CONTACTS);
    }

    private void offerWriteingSettingPermission(int requestCode){
        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
        startActivityForResult(intent,requestCode);
    }

    private void offerNotificationAccessPermission(int requestCode){
        Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.setData(Uri.parse("package:" + this.getPackageName()));
        startActivityForResult(intent,requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case PERMISSION_REQUEST_CONTACTS:
                //mForbiddenPermissionList.clear();
                if (permissions != null && permissions.length > 0
                        && isAllGranted(permissions)) {
                    checkbox.setChecked(true);
                    DialerApplication.unsatisfiedCallPermissions.clear();
                }

            break;
            case PERMISSION_REQUEST_AUTO_START:
                if(isAllGranted(permissions)) {
                    checkbox.setChecked(true);
                    DialerApplication.unsatisfiedrebootPermissions.clear();
                }
                break;

            case PERMISSIONS_REQUEST_NOTIFICATION_ACCESS:
                if(isAllGranted(permissions)) {
                   offerNotificationAccessPermission(PERMISSIONS_REQUEST_NOTIFICATION_ACCESS);
                }
                break;
        }

        verify();


    }

    private void verify() {
        if(DialerApplication.unsatisfiedCallPermissions.isEmpty() &&
                DialerApplication.unsatisfiedrebootPermissions.isEmpty() &&
                DialerApplication.unsatisfiednotificationPermissions.isEmpty() &&
                DialerApplication.unsatisfiedsettingPermissions.isEmpty()) {
            hopeinButton.setEnabled(true);
            hopeinButton.setBackground(getResources().getDrawable(com.batooni.dialer.R.drawable.batooni_round_selected_button));

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode){
            case  PERMISSION_REQUEST_DIALER_REPLACEMENT:
                offerContactReadAndWritePermission();
                break;
            case  PERMISSIONS_REQUEST_NOTIFICATION_ACCESS:
                if(resultCode == RESULT_OK) {
                    checkbox.setChecked(true);
                    DialerApplication.unsatisfiednotificationPermissions.clear();
                }
                break;
            case  PERMISSIONS_REQUEST_WRITE_SETTINGS:
                if(resultCode == RESULT_OK) {
                    checkbox.setChecked(true);
                    DialerApplication.unsatisfiedsettingPermissions.clear();
                }
                break;

        }

        verify();
    }

    private boolean isAllGranted(String permissions[] ) {
        boolean isAllGrand = true;
        for (int i = 0; i < permissions.length; i++) {
            if (checkSelfPermission(permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }





    /*private void checkSetDefaultDialerResult(int resultCode){
        String text="";
        switch(resultCode ){
            case RESULT_OK:
                text = "User accepted request to become default dialer";
                checkbox.setChecked(true);
                finish();
                break;
            case RESULT_CANCELED:
                text="User declined request to become default dialer";
                checkbox.setChecked(false);
                break;
            default :
                text="Unexpected result code $resultCode";
        }

        requestNecessaryRequiredPermissions();
    }*/

    private void requestNecessaryRequiredPermissions(){

        final ArrayList<String> unsatisfiedPermissions = new ArrayList<>();
        for (String permission : PermissionsUtil.sRequiredPermissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                unsatisfiedPermissions.add(permission);
            }
        }
        if (unsatisfiedPermissions.size() == 0) {
            finish();
            return;
        }
        requestPermissions(unsatisfiedPermissions.toArray(new String[unsatisfiedPermissions
                .size()]), PERMISSIONS_REQUEST_ALL_PERMISSIONS);
    }

    public static String[] GetStringArray(ArrayList<String> arr)
    {

        // declaration and initialise String Array
        String str[] = new String[arr.size()];

        // Convert ArrayList to object array
        Object[] objArr = arr.toArray();

        // Iterating and converting to String
        int i = 0;
        for (Object obj : objArr) {
            str[i++] = (String)obj;
        }

        return str;
    }

    private void launchDialerApp(){
        if(checkbox !=null)
            checkbox.setChecked(true);
        hopeinButton.setBackgroundColor(getResources().getColor(R.color.batooni_theme_color));
        hopeinButton.setEnabled(true);
        DialerApplication.clearall();
    }

    private boolean isNLServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(this.ACTIVITY_SERVICE);

        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (com.batooni.incallui.service.NotificationListener.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }


}
