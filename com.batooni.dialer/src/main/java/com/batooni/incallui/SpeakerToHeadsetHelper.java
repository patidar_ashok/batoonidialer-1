package com.batooni.incallui;

import android.content.Context;

/*
* Add for hands-free switch to headset
* */
public class SpeakerToHeadsetHelper {

    private static final String TAG = "SpeakerToHeadsetHelper";
    static SpeakerToHeadsetHelper mInstance;

    public SpeakerToHeadsetHelper() {
    }

    public static SpeakerToHeadsetHelper getInstance(Context context) {
        if (mInstance != null) {
            return mInstance;
        }

        Log.d(TAG, "getInstance [" + mInstance + "]");
        return mInstance;
    }

    public void init(Context context, CallButtonFragment callButtonFragment) {
    }

    public void unRegisterSpeakerTriggerListener() {
    }
}