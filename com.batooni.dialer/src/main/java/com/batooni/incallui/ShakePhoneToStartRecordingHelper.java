package com.batooni.incallui;

import android.content.Context;

/**
 * Add for shaking phone to start recording.
 */

public class ShakePhoneToStartRecordingHelper {

    private static final String TAG = "ShakePhoneToStartRecordingHelper";
    static ShakePhoneToStartRecordingHelper mInstance;

    public ShakePhoneToStartRecordingHelper() {
    }

    public static ShakePhoneToStartRecordingHelper getInstance(Context context) {
        if (mInstance != null) {
            return mInstance;
        }

        Log.d(TAG, "getInstance [" + mInstance + "]");
        return mInstance;
    }

    public void init(Context context, CallButtonFragment callButtonFragment) {
    }

    public void unRegisterTriggerRecorderListener() {
    }
}
