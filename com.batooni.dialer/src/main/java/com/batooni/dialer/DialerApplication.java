/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.batooni.dialer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Trace;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.telecom.TelecomManager;

import com.android.contacts.common.extensions.ExtensionsFactory;
import com.android.contacts.common.testing.NeededForTesting;
import com.android.contacts.common.util.PermissionsUtil;
import com.batooni.dialer.database.FilteredNumberAsyncQueryHandler;
import com.batooni.dialer.filterednumber.BlockedNumbersAutoMigrator;
import com.batooni.permission.PermissionItem;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;

public class DialerApplication extends Application {

    private static final String TAG = "DialerApplication";

    private static Context sContext;

    public static ArrayList<String>  unsatisfiedCallPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiedrebootPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiedsettingPermissions = new ArrayList<>();
    public static ArrayList<String>  unsatisfiednotificationPermissions = new ArrayList<>();
    String[] requiredPermissions;


    @Override
    public void onCreate() {
        sContext = this;
        Trace.beginSection(TAG + " onCreate");
        super.onCreate();
        Trace.beginSection(TAG + " ExtensionsFactory initialization");
        ExtensionsFactory.init(getApplicationContext());
        Trace.endSection();
        new BlockedNumbersAutoMigrator(PreferenceManager.getDefaultSharedPreferences(this),
                new FilteredNumberAsyncQueryHandler(getContentResolver())).autoMigrate();
        Trace.endSection();

        requiredPermissions = PermissionsUtil.sRequiredPermissions;

        requestNecessaryRequiredPermissions();

    }

    @Nullable
    public static Context getContext() {
        return sContext;
    }

    @NeededForTesting
    public static void setContextForTest(Context context) {
        sContext = context;
    }

    private void requestNecessaryRequiredPermissions(){

        if(needOfeplacingDefaultDialer())
            unsatisfiedCallPermissions.add((PermissionsUtil.DEFAULT_DIALER));

        for (String permission : PermissionsUtil.sRequiredPermissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                unsatisfiedCallPermissions.add(permission);
            }
        }

        for (String permission : PermissionsUtil.bRequiredPermissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                unsatisfiedrebootPermissions.add(permission);
            }
        }

        /*for (String permission : PermissionsUtil.settingRequiredPermissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                unsatisfiedsettingPermissions.add(permission);
            }
        }

        for (String permission : PermissionsUtil.notificationRequiredPermissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                unsatisfiednotificationPermissions.add(permission);
            }
        }*/
    }

    private boolean needOfeplacingDefaultDialer() {
        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

        if (!getPackageName().equals(telecomManager.getDefaultDialerPackage())) {
           return true;
        }
        return false;
    }

    public static void clearall(){
        unsatisfiedCallPermissions.clear();
        unsatisfiednotificationPermissions.clear();
        unsatisfiedsettingPermissions.clear();
        unsatisfiedrebootPermissions.clear();
    }



}
