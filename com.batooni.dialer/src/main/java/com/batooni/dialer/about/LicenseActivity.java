/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.batooni.dialer.about;

/**
 * Displays the licenses for all open source libraries.
 */
public class LicenseActivity extends android.app.Activity {
    private static final String LICENSE_FILE = "https://raw.githubusercontent.com/ernitinjai/ernitinjai.github.io/master/license.html";
    private android.webkit.WebView mWebView;

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.android.contacts.common.R.layout.licenses);
        mWebView = (android.webkit.WebView) findViewById(com.android.contacts.common.R.id.webview);
        mWebView.loadUrl(LICENSE_FILE);
        final android.app.ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(android.app.ActionBar.DISPLAY_HOME_AS_UP, android.app.ActionBar.DISPLAY_HOME_AS_UP);
        }
    }

    @Override
    protected void onDestroy() {
        mWebView.destroy();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
